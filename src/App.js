import React from 'react';
import './App.css';
import Recipe from "./Recipe"

const App = () => {
  const APP_ID = "9db14dd5"
  const APP_KEY = "fe4a00d3e0b6f8f2f8d28db419548b59"

  const [search,setSearch] = React.useState("")
  const [recipe,setRecipe] = React.useState([])
  const [query,setQuery] = React.useState("chicken")

  React.useEffect(() =>{
    getRecipe()
  },[query])

  const getRecipe = async () => {
    const response = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`);
    const data  = await response.json()
    setRecipe(data.hits)
  }

  const updateSearch = event => {
    setSearch(event.target.value)

  }

  const getSearch = event => {
    event.preventDefault();
    setQuery(search)
    setSearch('')
  }

  return(
      <div>
        <form className = "search-form" onSubmit={getSearch}>
          <input
            className="search-bar"
            type="text"
            placeholder="search any recipe here.."
            value={search}
            onChange={updateSearch}
          />
          <button className="search-button">Search!</button>
        </form>
        <div className ="flex-container">
            {recipe.map(recipe => (
                <Recipe
                  key={recipe.recipe.label}
                  label={recipe.recipe.label}
                  calories={recipe.recipe.calories}
                  image={recipe.recipe.image}
                  ingredients={recipe.recipe.ingredients}
                />
            ))}
          </div>
      </div>
    );
}

export default App;
