import React from "react"
const Recipe = (props) => {
  return(
    <div className ="recipes">
      <h1>{props.label}</h1>
      <h4>Calories: ({props.calories})</h4>
      <img src={props.image} alt=""/>
      <ol>
        {props.ingredients.map(ingredient => (
          <li>{ingredient.text}</li>
          ))}
      </ol>
    </div>
  )
}

export default Recipe